package com.example.clevertaskfinaljava.di;

import android.content.Context;

import com.example.clevertaskfinaljava.App;
import com.example.clevertaskfinaljava.retrofit.MainApiService;
import com.example.clevertaskfinaljava.retrofit.MainRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {ViewModelModule.class})
public class AppModule {

    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    MainApiService provideBankPointsApiService(Retrofit retrofit) {
        return retrofit.create(MainApiService.class);
    }

    @Provides
    MainRepository provideBankPointsRepository(MainApiService apiService) {
        return new MainRepository(apiService);
    }
}
