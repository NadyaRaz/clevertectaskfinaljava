package com.example.clevertaskfinaljava.di;

import com.example.clevertaskfinaljava.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract MainFragment contributeMainFragment();
}
