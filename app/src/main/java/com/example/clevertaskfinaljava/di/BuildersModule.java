package com.example.clevertaskfinaljava.di;

import com.example.clevertaskfinaljava.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindActivity();
}
