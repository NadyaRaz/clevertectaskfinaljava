package com.example.clevertaskfinaljava;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clevertaskfinaljava.dto.MetaFieldModel;
import com.example.clevertaskfinaljava.dto.MetaFieldType;
import com.example.clevertaskfinaljava.dto.MetaFieldValueModel;

import java.util.HashMap;
import java.util.List;

public class ElementAdapter extends RecyclerView.Adapter<ElementAdapter.Holder> {

    private final int TYPE_TEXT = 0;
    private final int TYPE_NUMERIC = 1;
    private final int TYPE_LIST = 2;

    private List<MetaFieldModel> list;
    public HashMap<String, String> request = new HashMap<>();

    public ElementAdapter(List<MetaFieldModel> list) {
        this.list = list;
        request.clear();
        for (MetaFieldModel model : list) {
            String value = "";
            if (model.getType() == MetaFieldType.LIST) {
                value = model.getValues().get(0).getKey();
            }
            request.put(model.getFiledName(), value);
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_TEXT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerview_elem_text, parent, false);
                break;
            case TYPE_NUMERIC:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerview_elem_numeric, parent, false);
                break;
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerview_elem_spinner, parent, false);
                break;
        }
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.populate(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()) {
            case TEXT:
                return TYPE_TEXT;
            case NUMERIC:
                return TYPE_NUMERIC;
            default:
                return TYPE_LIST;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {

        private View itemView;
        private TextView title;
        private EditText editString;
        private EditText editNumeric;
        private Spinner spinner;
        private String programName;
        private MetaFieldType type;

        public Holder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            title = itemView.findViewById(R.id.title);
            editString = itemView.findViewById(R.id.edit_line_string);
            editNumeric = itemView.findViewById(R.id.edit_line_numeric);
            spinner = itemView.findViewById(R.id.spinner);
        }

        void populate(final MetaFieldModel metaField) {
            programName = metaField.getFiledName();
            type = metaField.getType();
            title.setText(metaField.getTitle());
            switch (type) {
                case NUMERIC:
                    editNumeric.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            ElementAdapter.this.request.put(programName, s.toString());
                        }
                    });
                    break;
                case TEXT:
                    editString.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            ElementAdapter.this.request.put(programName, s.toString());
                        }
                    });
                    break;
                case LIST:
                    ArrayAdapter<MetaFieldValueModel> adapter = new ArrayAdapter<MetaFieldValueModel>(itemView.getContext(), R.layout.spinner_element, metaField.getValues());
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ElementAdapter.this.request.put(programName, ((MetaFieldValueModel) spinner.getSelectedItem()).getKey());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    break;
            }
        }
    }
}
