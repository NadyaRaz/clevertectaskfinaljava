package com.example.clevertaskfinaljava.dto;

import java.util.List;

public class MetaFieldModel {

    private String title;
    private String filedName;
    private MetaFieldType type;
    private List<MetaFieldValueModel> values;

    public MetaFieldModel(String title, String filedName, MetaFieldType type, List<MetaFieldValueModel> values) {
        this.title = title;
        this.filedName = filedName;
        this.type = type;
        this.values = values;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFiledName() {
        return filedName;
    }

    public void setFiledName(String filedName) {
        this.filedName = filedName;
    }

    public MetaFieldType getType() {
        return type;
    }

    public void setType(MetaFieldType type) {
        this.type = type;
    }

    public List<MetaFieldValueModel> getValues() {
        return values;
    }

    public void setValues(List<MetaFieldValueModel> values) {
        this.values = values;
    }
}
