package com.example.clevertaskfinaljava.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MetaDto {

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("fields")
    private List<MetaFieldDto> fields;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<MetaFieldDto> getFields() {
        return fields;
    }

    public void setFields(List<MetaFieldDto> fields) {
        this.fields = fields;
    }
}
