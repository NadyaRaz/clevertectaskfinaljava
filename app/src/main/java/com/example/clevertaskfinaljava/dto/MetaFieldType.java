package com.example.clevertaskfinaljava.dto;

import com.google.gson.annotations.SerializedName;

public enum MetaFieldType {

    @SerializedName("TEXT")
    TEXT,

    @SerializedName("NUMERIC")
    NUMERIC,

    @SerializedName("LIST")
    LIST
}
