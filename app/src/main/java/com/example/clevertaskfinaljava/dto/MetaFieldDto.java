package com.example.clevertaskfinaljava.dto;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class MetaFieldDto {

    @SerializedName("title")
    private String title;

    @SerializedName("name")
    private String filedName;

    @SerializedName("type")
    private MetaFieldType type;

    @SerializedName("values")
    private HashMap<String, String> values;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFiledName() {
        return filedName;
    }

    public void setFiledName(String filedName) {
        this.filedName = filedName;
    }

    public MetaFieldType getType() {
        return type;
    }

    public void setType(MetaFieldType type) {
        this.type = type;
    }

    public HashMap<String, String> getValues() {
        return values;
    }

    public void setValues(HashMap<String, String> values) {
        this.values = values;
    }
}
