package com.example.clevertaskfinaljava.dto;

import java.util.List;

public class MetaModel {

    private String title;
    private String image;
    private List<MetaFieldModel> fields;

    public MetaModel(String title, String image, List<MetaFieldModel> fields) {
        this.title = title;
        this.image = image;
        this.fields = fields;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<MetaFieldModel> getFields() {
        return fields;
    }

    public void setFields(List<MetaFieldModel> fields) {
        this.fields = fields;
    }
}
