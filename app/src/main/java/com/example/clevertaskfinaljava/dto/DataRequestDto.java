package com.example.clevertaskfinaljava.dto;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class DataRequestDto {

    @SerializedName("form")
    private HashMap<String, String> form;

    public DataRequestDto(HashMap<String, String> form) {
        this.form = form;
    }

    public HashMap<String, String> getForm() {
        return form;
    }

    public void setForm(HashMap<String, String> form) {
        this.form = form;
    }
}
