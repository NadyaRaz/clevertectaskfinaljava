package com.example.clevertaskfinaljava.dto;

import com.google.gson.annotations.SerializedName;

public class DataResponseDto {

    @SerializedName("result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
