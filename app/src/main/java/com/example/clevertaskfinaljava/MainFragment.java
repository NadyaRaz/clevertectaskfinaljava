package com.example.clevertaskfinaljava;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.clevertaskfinaljava.dto.DataRequestDto;
import com.example.clevertaskfinaljava.dto.MetaModel;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.DaggerFragment;

public class MainFragment extends DaggerFragment {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainViewModel viewModel;

    private ProgressBar progressBar;
    private ImageView imageView;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private Button sendButton;

    public static MainFragment getInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(requireActivity(), viewModelFactory).get(MainViewModel.class);
        progressBar = view.findViewById(R.id.main_fragment_progress_bar);
        imageView = view.findViewById(R.id.main_fragment_image);
        toolbar = view.findViewById(R.id.toolbar_headline);
        recyclerView = view.findViewById(R.id.recycler_view);
        sendButton = view.findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.sendData(new DataRequestDto(((ElementAdapter) recyclerView.getAdapter()).request));
            }
        });
        subscribe();
        viewModel.getMeta();
    }

    private void subscribe() {
        viewModel.metaLiveData.observe(this, new Observer<MetaModel>() {
            @Override
            public void onChanged(MetaModel metaModel) {
                toolbar.setTitle(metaModel.getTitle());
                recyclerView.setAdapter(new ElementAdapter(
                        metaModel.getFields()
                ));
                Glide.with(requireContext())
                        .load(metaModel.getImage())
                        .into(imageView);
            }
        });
        viewModel.progressLiveData.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean value) {
                if (value) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
        viewModel.resultLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                new AlertDialog.Builder(requireContext())
                        .setTitle(R.string.result)
                        .setMessage(s)
                        .show();
            }
        });
        viewModel.errorLiveData.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Toast.makeText(getContext(), R.string.error_message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
