package com.example.clevertaskfinaljava.retrofit;

import com.example.clevertaskfinaljava.dto.DataRequestDto;
import com.example.clevertaskfinaljava.dto.DataResponseDto;
import com.example.clevertaskfinaljava.dto.MetaDto;
import com.example.clevertaskfinaljava.dto.MetaFieldDto;
import com.example.clevertaskfinaljava.dto.MetaFieldModel;
import com.example.clevertaskfinaljava.dto.MetaFieldType;
import com.example.clevertaskfinaljava.dto.MetaFieldValueModel;
import com.example.clevertaskfinaljava.dto.MetaModel;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.functions.Function;

public class MainRepository {

    private MainApiService apiService;

    @Inject
    public MainRepository(MainApiService apiService) {
        this.apiService = apiService;
    }

    public Single<MetaModel> getMeta() {
        return apiService.getMeta().map(new Function<MetaDto, MetaModel>() {
            @Override
            public MetaModel apply(MetaDto metaDto) throws Exception {
                ArrayList<MetaFieldModel> metaFieldModels = new ArrayList<>();
                for (MetaFieldDto metaField : metaDto.getFields()) {
                    ArrayList<MetaFieldValueModel> values = null;
                    if (metaField.getType() == MetaFieldType.LIST) {
                        values = new ArrayList<>();
                        for (Map.Entry<String, String> entry : metaField.getValues().entrySet()) {
                            values.add(new MetaFieldValueModel(entry.getKey(), entry.getValue()));
                        }
                    }
                    metaFieldModels.add(new MetaFieldModel(metaField.getTitle(), metaField.getFiledName(), metaField.getType(), values));
                }
                return new MetaModel(metaDto.getTitle(), metaDto.getImage(), metaFieldModels);
            }
        });
    }

    public Single<String> sendData(DataRequestDto dataRequestDto) {
        return apiService.sendData(dataRequestDto).map(new Function<DataResponseDto, String>() {
            @Override
            public String apply(DataResponseDto dataResponseDto) throws Exception {
                return dataResponseDto.getResult();
            }
        });
    }
}
