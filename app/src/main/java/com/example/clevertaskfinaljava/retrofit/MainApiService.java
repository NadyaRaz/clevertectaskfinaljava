package com.example.clevertaskfinaljava.retrofit;

import com.example.clevertaskfinaljava.dto.DataRequestDto;
import com.example.clevertaskfinaljava.dto.DataResponseDto;
import com.example.clevertaskfinaljava.dto.MetaDto;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MainApiService {

    @GET("meta")
    Single<MetaDto> getMeta();

    @POST("data")
    Single<DataResponseDto> sendData(@Body DataRequestDto request);
}
