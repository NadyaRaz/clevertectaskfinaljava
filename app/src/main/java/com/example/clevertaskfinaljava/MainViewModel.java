package com.example.clevertaskfinaljava;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.clevertaskfinaljava.dto.DataRequestDto;
import com.example.clevertaskfinaljava.dto.MetaModel;
import com.example.clevertaskfinaljava.retrofit.MainRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    public MutableLiveData<MetaModel> metaLiveData = new MutableLiveData<>();
    public MutableLiveData<Boolean> progressLiveData = new MutableLiveData<>();
    public MutableLiveData<String> resultLiveData = new MutableLiveData<>();
    public MutableLiveData<Boolean> errorLiveData = new MutableLiveData<>();

    private MainRepository repository;

    private CompositeDisposable disposables = new CompositeDisposable();

    @Inject
    public MainViewModel(MainRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }

    public void getMeta() {
        disposables.add(
                repository.getMeta()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> {
                            progressLiveData.setValue(true);
                        })
                        .subscribe(
                                meta -> {
                                    metaLiveData.setValue(meta);
                                    progressLiveData.setValue(false);
                                },
                                error -> {
                                    errorLiveData.setValue(true);
                                    progressLiveData.setValue(false);
                                }
                        )
        );
    }

    public void sendData(DataRequestDto dataRequestDto) {
        disposables.add(
                repository.sendData(dataRequestDto)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> {
                            progressLiveData.setValue(true);
                        })
                        .subscribe(
                                result -> {
                                    resultLiveData.setValue(result);
                                    progressLiveData.setValue(false);
                                },
                                error -> {
                                    errorLiveData.setValue(true);
                                    progressLiveData.setValue(false);
                                }
                        )
        );
    }
}
